import { Component, OnInit } from '@angular/core';
import { CardService } from './card/card.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  
  public title = 'Laboratorio 2';
  public cardList:string[];

  constructor(private cardService: CardService){
    this.cardList = [];
  }

  ngOnInit(){
    this.generateCards();
    this.cardList = this.cardService.cardList;
  }
  
  public generateCards(){
    for (let index = 0; index < 2; index++) {
      const valor= 'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Voluptatum inventore reprehenderit aliquam eos deleniti beatae, alaaaaaaaias, accusantium exercitationem repellendus illum autem asperiores consequuntur, delectus odit eaque aperiam culpa ex vel.';
      this.cardService.pushCard(valor);
    }
  }
}

