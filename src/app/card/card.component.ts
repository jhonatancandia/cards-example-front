import { Component, OnInit, Input } from '@angular/core';
import { CardService } from './card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  
  @Input() public id:number;
  @Input() public content:string;
  @Input() public date:number;

  constructor() { 
    this.id = 0;
    this.content = ''; 
    this.date = 0;
  }

  ngOnInit() {
    
  }

}
