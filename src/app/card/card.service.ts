import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CardService {
  private list: string[];

  constructor() {
    this.list = [];
  }

  public get cardList(): string[]{
    return this.list;
  }

  public set cardList(listCards: string[]){
    this.list = listCards;
  }

  public pushCard(value:string){
    this.list.push(value);
  }

}
